import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = (props) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.buttonStyle}>
      <Text style={styles.textStyle}>
        {props.children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
buttonStyle: {
  flex: 1,
  alignSelf: 'stretch',
  backgroundColor: '#fff',
  borderColor: '#007aff',
  borderWidth: 1,
  marginLeft: 5,
  marginRight: 5,
},
textStyle:{
  alignSelf: 'center',
  color: '#007aff',
  fontSize: 16,
  fontWeight: 'bold',
  paddingTop: 10,
  paddingBottom: 10

}

};



export default Button;
