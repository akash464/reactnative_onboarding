// Android device code place here...
import React from 'react';
import { AppRegistry, View, StyleSheet } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

//Create components
const App = () => (
  <View style={StyleSheet.absoluteFill}>
      <Header headerText={'Albums'} />
      <AlbumList />
  </View>
);

//render it to device
AppRegistry.registerComponent('FirstDemo', () => App);
